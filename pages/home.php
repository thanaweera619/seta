

<div class="app-main__outer">
    <div id="refreshData">
        <div class="app-main__inner">
            <div class="app-page-title">
                <div class="page-title-wrapper">
                    <div class="page-title-heading"><b>SECURITY EDUCATION TRAINING AWARENESS</b></div>
                </div>
            </div>
            <div class="page-title-subheading" style="text-align: justify; font-size: medium">SETA is a program designed to help organizations to mitigate the number of security breaches caused by human error. This is accomplished by making people aware of information security policies and
                being able to apply it during their daily activities to help prevent security incidents. Choose from a variety of learning platforms to keep up to date with changing policies, procedures and security environments and meet your performance requirements.
                Use our resources to bring security expertise straight to your organization.
            </div>
            <img class="banner" alt="banner" src="assets/images/banner.png">
        </div>
    </div>
</div>