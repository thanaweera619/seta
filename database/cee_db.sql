-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2022 at 09:33 PM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cee_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_acc`
--

CREATE TABLE `admin_acc` (
  `admin_id` int(11) NOT NULL,
  `admin_user` varchar(1000) NOT NULL,
  `admin_pass` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_acc`
--

INSERT INTO `admin_acc` (`admin_id`, `admin_user`, `admin_pass`) VALUES
(1, 'admin@username', 'admin@password');

-- --------------------------------------------------------

--
-- Table structure for table `course_tbl`
--

CREATE TABLE `course_tbl` (
  `cou_id` int(11) NOT NULL,
  `cou_name` varchar(1000) NOT NULL,
  `cou_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_tbl`
--

INSERT INTO `course_tbl` (`cou_id`, `cou_name`, `cou_created`) VALUES
(69, 'NEW LIFE HOSPITAL', '2022-12-09 14:00:17');

-- --------------------------------------------------------

--
-- Table structure for table `examinee_tbl`
--

CREATE TABLE `examinee_tbl` (
  `exmne_id` int(11) NOT NULL,
  `exmne_fullname` varchar(1000) NOT NULL,
  `exmne_course` varchar(1000) NOT NULL,
  `exmne_gender` varchar(1000) NOT NULL,
  `exmne_birthdate` varchar(1000) NOT NULL,
  `exmne_year_level` varchar(1000) NOT NULL,
  `exmne_email` varchar(1000) NOT NULL,
  `exmne_password` varchar(1000) NOT NULL,
  `exmne_status` varchar(1000) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examinee_tbl`
--

INSERT INTO `examinee_tbl` (`exmne_id`, `exmne_fullname`, `exmne_course`, `exmne_gender`, `exmne_birthdate`, `exmne_year_level`, `exmne_email`, `exmne_password`, `exmne_status`) VALUES
(4, 'Rogz Nunezsss', '69', 'male', '2019-11-15', 'third year', 'rogz.nunez2013@gmail.com', 'rogz', 'active'),
(5, 'Jane Rivera', '69', 'female', '2019-11-14', 'second year', 'jane@gmail.com', 'jane', 'active'),
(6, 'Glenn Duerme', '69', 'female', '2019-12-24', 'third year', 'glenn@gmail.com', 'glenn', 'active'),
(7, 'Maria Duerme', '69', 'female', '2018-11-25', 'second year', 'maria@gmail.com', 'maria', 'active'),
(8, 'Dave Limasac', '69', 'female', '2019-12-21', 'first year', 'dave@gmail.com', 'dave', 'active'),
(9, 'Vinod Kavishka', '69', 'male', '1983-06-22', 'third year', 'ostf00vino@newlife.com', 'vino', 'active'),
(10, 'Jefry Fernando', '69', 'male', '1991-10-06', 'third year', 'ostf00jefry@newlife.com', 'jefry', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `exam_answers`
--

CREATE TABLE `exam_answers` (
  `exans_id` int(11) NOT NULL,
  `axmne_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `quest_id` int(11) NOT NULL,
  `exans_answer` varchar(1000) NOT NULL,
  `exans_status` varchar(1000) NOT NULL DEFAULT 'new',
  `exans_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_answers`
--

INSERT INTO `exam_answers` (`exans_id`, `axmne_id`, `exam_id`, `quest_id`, `exans_answer`, `exans_status`, `exans_created`) VALUES
(328, 7, 26, 32, 'No this is kinda good to be true', 'old', '2022-12-09 16:52:21'),
(329, 6, 26, 39, 'No', 'old', '2022-12-09 16:47:45'),
(330, 6, 26, 38, 'Verify their identity with the IT department then let him do whatever ', 'old', '2022-12-09 16:47:45'),
(331, 6, 26, 36, 'Give permission because the program might be important ', 'new', '2022-12-09 16:47:45'),
(332, 6, 26, 35, 'On an encrypted database ', 'new', '2022-12-09 16:47:45'),
(333, 6, 26, 34, 'Wont violate HIPAA rules ', 'new', '2022-12-09 16:47:45'),
(334, 6, 26, 41, 'Use password safe', 'new', '2022-12-09 16:47:45'),
(335, 6, 26, 44, 'No', 'new', '2022-12-09 16:47:45'),
(336, 6, 26, 42, 'Sometimes when I’m bored ', 'new', '2022-12-09 16:47:45'),
(337, 6, 26, 40, 'Yes', 'new', '2022-12-09 16:47:45'),
(338, 6, 26, 33, 'Yes true', 'new', '2022-12-09 16:47:45'),
(339, 6, 26, 38, 'Verify their identity with the IT department and let him do this work but keep inspecting his actions', 'new', '2022-12-09 16:47:45'),
(340, 6, 26, 43, 'Yeah I do', 'new', '2022-12-09 16:47:45'),
(341, 6, 26, 37, 'Plug it and check for the owner', 'new', '2022-12-09 16:47:46'),
(342, 6, 26, 39, 'Yeah I kinda do even if its prohibited ', 'new', '2022-12-09 16:47:46'),
(343, 7, 26, 35, 'On an encrypted database ', 'new', '2022-12-09 16:52:21'),
(344, 7, 26, 44, 'No', 'new', '2022-12-09 16:52:21'),
(345, 7, 26, 41, 'Use password safe', 'new', '2022-12-09 16:52:21'),
(346, 7, 26, 39, 'Yeah I kinda do even if its prohibited ', 'new', '2022-12-09 16:52:21'),
(347, 7, 26, 38, 'Verify their identity with the IT department and let him do this work but keep inspecting his actions', 'new', '2022-12-09 16:52:21'),
(348, 7, 26, 42, 'No', 'new', '2022-12-09 16:52:21'),
(349, 7, 26, 33, 'No this is kinda good to be true ', 'new', '2022-12-09 16:52:21'),
(350, 7, 26, 37, 'Take it home with you (its not stealing if you found it', 'new', '2022-12-09 16:52:21'),
(351, 7, 26, 40, 'Yes', 'new', '2022-12-09 16:52:21'),
(352, 7, 26, 34, 'Wont violate HIPAA rules ', 'new', '2022-12-09 16:52:21'),
(353, 7, 26, 43, 'Yeah I do', 'new', '2022-12-09 16:52:22'),
(354, 7, 26, 36, 'Give permission because the program might be important ', 'new', '2022-12-09 16:52:22'),
(355, 9, 26, 38, 'Let them do their job', 'new', '2022-12-09 16:53:21'),
(356, 9, 26, 44, 'Yeah ', 'new', '2022-12-09 16:53:21'),
(357, 9, 26, 42, 'No', 'new', '2022-12-09 16:53:21'),
(358, 9, 26, 35, 'On an encrypted database ', 'new', '2022-12-09 16:53:21'),
(359, 9, 26, 34, 'Not gonna snitch on a patient ', 'new', '2022-12-09 16:53:21'),
(360, 9, 26, 41, 'Use password safe', 'new', '2022-12-09 16:53:22'),
(361, 9, 26, 40, 'Yes', 'new', '2022-12-09 16:53:22'),
(362, 9, 26, 37, 'Plug it and check for the owner', 'new', '2022-12-09 16:53:22'),
(363, 9, 26, 33, 'Yes who wont do that', 'new', '2022-12-09 16:53:22'),
(364, 9, 26, 43, 'No', 'new', '2022-12-09 16:53:22'),
(365, 9, 26, 36, 'Give permission because the program might be important ', 'new', '2022-12-09 16:53:22'),
(366, 9, 26, 39, 'Yeah I kinda do even if its prohibited ', 'new', '2022-12-09 16:53:22'),
(367, 10, 26, 42, 'Sometimes when I’m bored ', 'new', '2022-12-09 20:14:07'),
(368, 10, 26, 37, 'Do not plug it. Just put a notice on the notice board you have it and owner can ', 'new', '2022-12-09 20:14:07'),
(369, 10, 26, 40, 'Yes', 'new', '2022-12-09 20:14:08'),
(370, 10, 26, 34, 'Wont violate HIPAA rules ', 'new', '2022-12-09 20:14:08'),
(371, 10, 26, 44, 'No', 'new', '2022-12-09 20:14:08'),
(372, 10, 26, 39, 'Yeah I kinda do even if its prohibited ', 'new', '2022-12-09 20:14:08'),
(373, 10, 26, 36, 'Wont give permission and scan for viruses right away and inform IT department', 'new', '2022-12-09 20:14:08'),
(374, 10, 26, 38, 'Verify their identity with the IT department and let him do this work but keep inspecting his actions', 'new', '2022-12-09 20:14:08'),
(375, 10, 26, 41, 'Use password safe', 'new', '2022-12-09 20:14:08'),
(376, 10, 26, 43, 'Yeah I do', 'new', '2022-12-09 20:14:08'),
(377, 10, 26, 35, 'On an encrypted database ', 'new', '2022-12-09 20:14:08'),
(378, 10, 26, 33, 'Yes true', 'new', '2022-12-09 20:14:08');

-- --------------------------------------------------------

--
-- Table structure for table `exam_attempt`
--

CREATE TABLE `exam_attempt` (
  `examat_id` int(11) NOT NULL,
  `exmne_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `examat_status` varchar(1000) NOT NULL DEFAULT 'used'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_attempt`
--

INSERT INTO `exam_attempt` (`examat_id`, `exmne_id`, `exam_id`, `examat_status`) VALUES
(59, 6, 26, 'used'),
(60, 7, 26, 'used'),
(61, 9, 26, 'used'),
(62, 10, 26, 'used');

-- --------------------------------------------------------

--
-- Table structure for table `exam_question_tbl`
--

CREATE TABLE `exam_question_tbl` (
  `eqt_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_question` varchar(1000) NOT NULL,
  `exam_ch1` varchar(1000) NOT NULL,
  `exam_ch2` varchar(1000) NOT NULL,
  `exam_ch3` varchar(1000) NOT NULL,
  `exam_ch4` varchar(1000) NOT NULL,
  `exam_answer` varchar(1000) NOT NULL,
  `exam_status` varchar(1000) NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_question_tbl`
--

INSERT INTO `exam_question_tbl` (`eqt_id`, `exam_id`, `exam_question`, `exam_ch1`, `exam_ch2`, `exam_ch3`, `exam_ch4`, `exam_answer`, `exam_status`) VALUES
(33, 26, 'You have gotten an email claiming you have win the newest iPhone. All you have to do is click the link. Would you do it?', 'Yes who wont do that', 'Yes true', 'No this is kinda good to be true ', 'All', 'No this is kinda good to be true ', 'active'),
(34, 26, 'Some stranger came to you claiming he is from law enforcement and he needs some information about a patient what would you do?', 'Not gonna snitch on a patient ', 'Wont violate HIPAA rules ', 'Be a law abiding citizen and help the officer ', 'All', 'Wont violate HIPAA rules ', 'active'),
(35, 26, 'How would you keep the patient data', 'On a file on my PC', 'On an encrypted database ', 'Plug it and check ', 'Uploading to public cloud', 'On an encrypted database ', 'active'),
(36, 26, 'There is a program installer asking your permission to deactivate windows firewall  and windows defender. What would you do?', 'Give permission because the program might be important ', 'Wont give permission and scan for viruses right away', 'Wont give permission and scan for viruses right away and inform IT department', 'All', 'Wont give permission and scan for viruses right away and inform IT department', 'active'),
(37, 26, 'You have found an unknown USB drive in the hospital. What would you do', 'Plug it and check for the owner', 'Take it home with you (its not stealing if you found it', 'Do not plug it. Just put a notice on the notice board you have it and owner can ', 'All', 'Take it home with you (its not stealing if you found it', 'active'),
(38, 26, 'If someone claiming that they are from IT department and they want to access your computer, what would you do', 'Let them do their job', 'Verify their identity with the IT department then let him do whatever ', 'Verify their identity with the IT department and let him do this work but keep inspecting his actions', 'Give permission because the program might be important ', 'Verify their identity with the IT department and let him do this work but keep inspecting his actions', 'active'),
(39, 26, 'Would you casually talk about specific patients at the hospital', 'Yeah I kinda do even if its prohibited ', 'No', '', '', 'No', 'active'),
(40, 26, 'Do you use password logins for your computers ', 'Yes', 'No', '', '', 'Yes', 'active'),
(41, 26, 'Where do you keep your passwords', 'Memorize it ', 'Keep it in a sticky note stick to your monitor frame ', 'Keep it under keyboard', 'Use password safe', 'Use password safe', 'active'),
(42, 26, 'Do you post pictures of your work station in Instagram stories ', 'No', 'Sometimes when I’m bored ', 'Most of the time', 'All time', 'Sometimes when I’m bored ', 'active'),
(43, 26, 'Do you update your software in your work pc', 'No', 'Too much of a hassle ', 'Yeah I do', '', 'Yeah I do', 'active'),
(44, 26, 'Do you post pictures of medical IOT devices on social media', 'Yeah ', 'No', '', '', 'No', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `exam_tbl`
--

CREATE TABLE `exam_tbl` (
  `ex_id` int(11) NOT NULL,
  `cou_id` int(11) NOT NULL,
  `ex_title` varchar(1000) NOT NULL,
  `ex_time_limit` varchar(1000) NOT NULL,
  `ex_questlimit_display` int(11) NOT NULL,
  `ex_description` varchar(10000) NOT NULL,
  `ex_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_tbl`
--

INSERT INTO `exam_tbl` (`ex_id`, `cou_id`, `ex_title`, `ex_time_limit`, `ex_questlimit_display`, `ex_description`, `ex_created`) VALUES
(26, 69, 'Hospital office staff', '20', 12, 'This quiz asks hypothetical questions and asks you to be honest in your responses. ', '2022-12-09 16:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks_tbl`
--

CREATE TABLE `feedbacks_tbl` (
  `fb_id` int(11) NOT NULL,
  `exmne_id` int(11) NOT NULL,
  `fb_exmne_as` varchar(1000) NOT NULL,
  `fb_feedbacks` varchar(1000) NOT NULL,
  `fb_date` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks_tbl`
--

INSERT INTO `feedbacks_tbl` (`fb_id`, `exmne_id`, `fb_exmne_as`, `fb_feedbacks`, `fb_date`) VALUES
(11, 6, 'Glenn Duerme', 'This Quiz is good ', 'December 09, 2022');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_acc`
--
ALTER TABLE `admin_acc`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `course_tbl`
--
ALTER TABLE `course_tbl`
  ADD PRIMARY KEY (`cou_id`);

--
-- Indexes for table `examinee_tbl`
--
ALTER TABLE `examinee_tbl`
  ADD PRIMARY KEY (`exmne_id`);

--
-- Indexes for table `exam_answers`
--
ALTER TABLE `exam_answers`
  ADD PRIMARY KEY (`exans_id`);

--
-- Indexes for table `exam_attempt`
--
ALTER TABLE `exam_attempt`
  ADD PRIMARY KEY (`examat_id`);

--
-- Indexes for table `exam_question_tbl`
--
ALTER TABLE `exam_question_tbl`
  ADD PRIMARY KEY (`eqt_id`);

--
-- Indexes for table `exam_tbl`
--
ALTER TABLE `exam_tbl`
  ADD PRIMARY KEY (`ex_id`);

--
-- Indexes for table `feedbacks_tbl`
--
ALTER TABLE `feedbacks_tbl`
  ADD PRIMARY KEY (`fb_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_acc`
--
ALTER TABLE `admin_acc`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course_tbl`
--
ALTER TABLE `course_tbl`
  MODIFY `cou_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `examinee_tbl`
--
ALTER TABLE `examinee_tbl`
  MODIFY `exmne_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `exam_answers`
--
ALTER TABLE `exam_answers`
  MODIFY `exans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;

--
-- AUTO_INCREMENT for table `exam_attempt`
--
ALTER TABLE `exam_attempt`
  MODIFY `examat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `exam_question_tbl`
--
ALTER TABLE `exam_question_tbl`
  MODIFY `eqt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `exam_tbl`
--
ALTER TABLE `exam_tbl`
  MODIFY `ex_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `feedbacks_tbl`
--
ALTER TABLE `feedbacks_tbl`
  MODIFY `fb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
